package ch.slomoz.methref;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.implementation.InvocationHandlerAdapter;
import net.bytebuddy.matcher.ElementMatchers;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Function;

/**
 * A utility class to resolve Method names by their reference, e.g. (Person::getFirstName).
 *
 * This internally creates a proxy of the given class to execute the method reference on it.
 * When invoking a method a MethodInterceptor retrieves the called method name. Method calls are not
 * propagated to the internal proxy.
 *
 * @param <T> Class for which this MethRef should work.
 */
public class MethRef<T> {

    private static class RecordingObject implements InvocationHandler {
        private String lastCalledMethodName = "";

        public String getLastCalledMethodName() {
            return lastCalledMethodName;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) {
            lastCalledMethodName = method.getName();
            // Since we never expose the proxy (as it is only used to track method calls), no propagation needed.
            return null;
        }
    }

    private final RecordingObject rec;
    private final T proxy;

    public MethRef(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        rec = new RecordingObject();
        proxy = new ByteBuddy()
                .subclass(clazz)
                .method(ElementMatchers.any())
                .intercept(InvocationHandlerAdapter.of(rec))
                .make()
                .load(clazz.getClassLoader())
                .getLoaded()
                .getDeclaredConstructor()
                .newInstance();
    }

    public String resolveName(Function<T,?> function) {
        function.apply(proxy);
        return rec.getLastCalledMethodName();
    }
}
