package ch.slomoz.methref;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;

public class MethRefTest {

    public static class TestClass {

        public TestClass() {
        }

        public String getName() {
            return "";
        }
    }

    @Test
    void testResolveName() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        MethRef<TestClass> personMethodNameResolver = new MethRef<>(TestClass.class);
        Assertions.assertEquals("getName", personMethodNameResolver.resolveName(TestClass::getName));
    }

}
